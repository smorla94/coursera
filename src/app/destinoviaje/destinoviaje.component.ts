import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Destinoviaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css']
})
export class DestinoviajeComponent implements OnInit {
  @Input()destino!: Destinoviaje;
  @HostBinding("attr.class") cssClass = "col-md-4"
  constructor() { 
  }

  ngOnInit(): void {
    this.destino.nombre
    console.log("hola ",this.destino.nombre)
  }

}
