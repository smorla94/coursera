import { Component, OnInit } from '@angular/core';
import { Destinoviaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-listadestino',
  templateUrl: './listadestino.component.html',
  styleUrls: ['./listadestino.component.css']
})
export class ListadestinoComponent implements OnInit {
  destinos: Destinoviaje[] = [];
  constructor() {
    
   }

  ngOnInit(): void {
  }

  guardar( nombre :string, url: string) :boolean{
    this.destinos.push(new Destinoviaje(nombre , url))
    return false;
  }

}
